module UrlsHelper

  #   Code based on https://gist.github.com/zumbojo/1073996

  ALPHABET =
      'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'.split(//)

  def alphabet
    ALPHABET
  end

  def encode(id)
    return ALPHABET[0] if id == 0
    encodedValue = ''
    base = ALPHABET.length
    while id > 0
      encodedValue << ALPHABET[id.modulo(base)]
      id /= base
    end
    encodedValue.reverse
  end

  def decode(encodedValue)
    id = 0
    base = ALPHABET.length
    encodedValue.each_char { |char| id = id * base + ALPHABET.index(char) }
    id
  end

  def validates_word?(shortUrl)
    /\A[a-zA-Z0-9]+\z/ === shortUrl
  end

end

class UrlsController < ApplicationController

  include UrlsHelper

  before_action 'url_params', only: [:create]

  def index
    @url = Url.new
  end

  def create

    url = Url.new(url_params)
    url.short = encode(Url.maximum(:id).next)

    if (url.save)
      flash[:success] = {long: url.long, tiny: root_url + url.short}
      redirect_to :action => 'index'
    else
      flash[:error] = 'Cannot create a tiny url, please enter a valid url'
      redirect_to :action => 'index'
    end

  end

  def tiny

    shortUrl = params[:tinyUrl]

    if validates_word?(shortUrl)
      id = decode(shortUrl)
      url = Url.find_by_id(id)
      if url
        redirect_to url.long
      else
        flash[:error] = "The url that you are looking for doesn't exist in our database"
        redirect_to :action => 'index'
      end
    else
      flash[:error] = 'The url that you sent is invalid. Make sure you typed it correctly'
      redirect_to :action => 'index'
    end

  end

  private

  def url_params
    params.require(:url).permit(:long)
  end

end
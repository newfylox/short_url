class Url < ActiveRecord::Base

  validates_presence_of :short, :long

  validates_format_of :short,:with => /\A[a-zA-Z0-9]+\z/

  validates_format_of :long, :with => URI.regexp

end

require 'rails_helper'

describe Url do

  let(:validLongUrl) { 'http://therealreal.com' }
  let(:validShortUrl) { 'Q3b' }
  let(:invalidLongUrl) { 'www.google..com' }
  let(:invalidShortUrl) { 'A9.' }

  context 'when create an empty url model' do

    let(:url) { Url.create }
    subject { url }

    it {
      expect(url).to_not be_valid
    }

    context 'with a valid short url given' do

      it 'should require long url' do
        url.short = validShortUrl
        expect(url.long).to be_nil
        expect(url).to_not be_valid
      end

    end

    context 'with a valid long url given' do

      it 'should require short url' do
        url.long = validLongUrl
        expect(url.short).to be_nil
        expect(url).to_not be_valid
      end

    end

  end

  context 'when create a filled url model' do

    let(:url) { Url.create short: invalidShortUrl, long: invalidLongUrl }
    subject { url }

    context 'with an invalid short url' do

      context 'when a valid long url is given' do

        it {
          url.long = validLongUrl
          expect(url).to_not be_valid
        }

      end

      context 'when an invalid long url is given' do

        it {
          expect(url).to_not be_valid
        }

      end


    end

    context 'with a valid short url' do

      before { url.short = validShortUrl }

      context 'when an invalid long url is given' do

        it {
          expect(url).to_not be_valid
        }

      end

      context 'when a valid long url is given' do

        it {
          url.long = validLongUrl
          expect { url.save }.to change { Url.count }.by(1)
          expect(url).to be_valid
        }

      end

    end

  end

end
require 'rails_helper'

describe 'urls/index.html.erb' do

  it 'should display a form' do

    assign(:url, Url.new)

    render

    expect(rendered).to have_selector('form')
    expect(rendered).to have_selector('input', count: 3)
    expect(rendered).to have_css('input[value="Tiny Me"]')
    expect(rendered).to have_css('label', :text => 'Url to shrink:')
    expect(rendered).to have_css('input[placeholder="Your url"]')

  end

end
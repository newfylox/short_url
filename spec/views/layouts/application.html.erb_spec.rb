require 'rails_helper'

describe 'layouts/application.html.erb' do

  it 'should have a title' do

    render

    expect(rendered).to have_title(/\ATiny Me\z/)

  end

  context 'when there is a flash notice' do

    context 'with an error' do

      it 'should show an error message' do

        flash[:error] = 'error'

        render

        expect(rendered).to match(/<div id="error">error<\/div>/)

      end

    end

    context 'with a success' do

      it 'should show a success message' do

        flash[:success] = 'success'

        render

        expect(rendered).to match(/<div class="row col-md-6 col-md-offset-3" id="success">/)
        expect(rendered).to have_css('label[id="success-long-label"]')
        expect(rendered).to have_css('input[id="success-long-input"]')
        expect(rendered).to have_css('label[id="success-tiny-label"]')
        expect(rendered).to have_css('input[id="success-tiny-input"]')

      end

    end

  end

end
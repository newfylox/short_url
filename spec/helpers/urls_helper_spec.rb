require 'rails_helper'

describe UrlsHelper do

  it { expect(alphabet).to eq("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".split(//)) }

  it { expect(validates_word?('41a.l')).to be_falsey }

  it { expect(validates_word?('a')).to be_truthy }

  describe '#encode' do

    it { expect(encode(0)).to eq('a') }

    it { expect(encode(1)).to eq('b') }

    it { expect(encode(1000)).to eq('qi') }

  end

  describe '#decode' do

    it { expect(decode('a')).to eq(0) }

    it { expect(decode('b')).to eq(1) }

    it { expect(decode('qi')).to eq(1000) }

    it { expect(decode('aW3v')).to eq(187943) }

  end

end
require 'rails_helper'

describe UrlsController do

  describe '#index' do

    context 'with a GET method' do

      before { get :index }

      it 'should respond' do
        expect(response).to be_success
      end

      it 'should render the :index view' do
        expect(response).to render_template :index
      end

    end

    context 'with other HTTP methods' do

      %w{post put delete patch}.each do |method|
        it "should not respond with #{ method }" do
          expect { send(method, :new) }.to raise_exception
        end
      end

    end

  end

  describe '#tiny' do

    fixtures :urls

    context 'with a :tinyUrl parameter' do

      context 'when it matches' do

        it 'should redirect to long url if record exists' do
          get :tiny, :tinyUrl => urls(:google).short
          expect(response).to redirect_to(urls(:google).long)
        end

        it 'should flash error and redirect if record does not exist' do
          get :tiny, :tinyUrl => 'd'
          expect(flash[:error]).to_not be_nil
          expect(response).to redirect_to(root_path)
        end

      end

      context 'when it does not match' do

        it 'should flash and redirect' do
          get :tiny, :tinyUrl => 'a3W.'
          expect(flash[:error]).to_not be_nil
          expect(response).to redirect_to(root_path)
        end

      end

    end

    context 'when no :tinyUrl parameter is given' do

      it 'should require the :tinyUrl parameter' do
        expect { get :tiny }.to raise_error(ActionController::UrlGenerationError)
      end

    end

  end

  describe '#create' do

    it 'should require the :longUrl parameter' do
      expect { post :create }.to raise_error(ActionController::ParameterMissing)
    end

    context 'with a valid url' do

      let(:longUrl) { 'http://therealreal.com' }
      let(:validParameter) { {:url => {long: longUrl}} }
      before { post :create, validParameter }

      it 'should flash a success' do
        expect(flash[:success][:long]).to eq(longUrl)
        expect(flash[:success][:tiny]).to_not be_nil
      end

      it 'should redirect' do
        expect(response).to redirect_to(root_path)
      end

    end

    context 'with an invalid url' do

      let(:longUrl) { 'www.google.com' }
      let(:invalidParameter) { {:url => {:long => longUrl} } }
      before { post :create, invalidParameter }

      it 'should flash an error' do
        expect(flash[:error]).to_not be_nil
      end

      it 'should redirect' do
        expect(response).to redirect_to(root_path)
      end

    end

  end

end
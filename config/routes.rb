Rails.application.routes.draw do

  root to: 'urls#index'

  get '/:tinyUrl', to: 'urls#tiny', as: 'tiny', constraints: {:tinyUrl => /[^\/]+/ }

  resources :urls, only: [:index, :create]

end